var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

(window.wpJsonpTemplateSections=window.wpJsonpTemplateSections||[]).push([[18],{998:function(n,t,e){"use strict";e.r(t);var i=e(8);e(0),t.default=function(n){new Promise((function(n,t){window.addEventListener("load",(function(){n()}))})).then((function(){var t=Object(i.a)(window,"Y.Squarespace.Singletons.ShoppingCart",null);t&&(t.on("item-added",(function(t){n.classList.remove("hidden")})),t.after("load",(function(t){t.target.get("totalQuantity")>0&&n.classList.remove("hidden")})))}))}}}]);

}
/*
     FILE ARCHIVED ON 20:42:11 Feb 11, 2022 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:26:44 Feb 12, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 159.229
  exclusion.robots: 0.159
  exclusion.robots.policy: 0.126
  cdx.remote: 0.087
  esindex: 0.01
  LoadShardBlock: 65.01 (3)
  PetaboxLoader3.datanode: 52.933 (4)
  CDXLines.iter: 41.61 (3)
  PetaboxLoader3.resolve: 53.256 (2)
  load_resource: 46.325
*/